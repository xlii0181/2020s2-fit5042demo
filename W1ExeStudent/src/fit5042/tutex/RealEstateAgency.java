package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
        Property p1 = new Property(1,"Australia",2,150,420000.00);
        Property p2 = new Property(2,"China",3,150,320000.00);
        Property p3 = new Property(3,"America",2,800,460000.00);
        Property p4 = new Property(4,"Australia",2,550,120000.00);
        Property p5 = new Property(5,"Australia",2,230,320000.00);
        try {
			propertyRepository.addProperty(p1);
			propertyRepository.addProperty(p2);
			propertyRepository.addProperty(p3);
			propertyRepository.addProperty(p4);
			propertyRepository.addProperty(p5);
			System.out.println("Five properties have been added successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
        try {
			for (int i = 0; i < propertyRepository.getAllProperties().size(); i++) {
				System.out.println(propertyRepository.getAllProperties().get(i).toString());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById(){
        System.out.println("Enter the ID of the property you want to search: ");
        int integer = inputInt();
        String inf = "";
		try {
			inf = propertyRepository.searchPropertyById(integer).toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println(inf);    
    }
    
    public String inputString(String displayMessage) {
        System.out.println(displayMessage);
        Scanner scan = new Scanner(System.in);
        String temp = scan.nextLine();
        return temp;
    }
    
    private int inputInt() {
        boolean notAnInt = true;
        int integer = 0;
        while (notAnInt) {
            Scanner sc = new Scanner(System.in);
            //judge whether the input is an int number
            try {
                integer = sc.nextInt();
                notAnInt = false;
            }
            //not an int number
            catch (Exception e) {
                System.out.println("Please enter right options");
            }
        }
        return integer;
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
